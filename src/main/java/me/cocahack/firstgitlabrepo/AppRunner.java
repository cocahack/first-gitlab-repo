package me.cocahack.firstgitlabrepo;

import lombok.AllArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AppRunner implements ApplicationRunner {

    private String appMessage;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println(appMessage);
    }
}
